ALTER USER 'root'@'localhost' IDENTIFIED BY 'root';
CREATE USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY 'root';
GRANT ALL ON *.* TO 'root'@'%';
GRANT ALL PRIVILEGES ON *.* TO  'root'@'%';
FLUSH privileges;
