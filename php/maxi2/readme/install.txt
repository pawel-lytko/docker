Rzeczy do zrobienia po instalacji kontenera

1. Zmiana hasła dla serwera My-SQL

Polecenia:
- docker network ls
- docker network inspect company_site_static_network
- Pobrać adres IP z klucza: IPAM->Config->Gateway

Ustalenie bieżacego hasła i uprawnień:
sudo docker logs db-container 2>&1 | grep GENERATED

sudo docker exec -it db-container mysql -u root -p

ALTER USER 'root'@'localhost' IDENTIFIED BY '[nowe hasło]';

CREATE USER 'root'@'[wpisać IP z IPAM->Config->Gateway]' IDENTIFIED BY '[nowe hasło]';

GRANT ALL ON *.* TO 'root'@'[wpisać IP z IPAM->Config->Gateway]';

GRANT ALL PRIVILEGES ON *.* TO  'root'@'localhost';
FLUSH privileges;

2. Ustawienia dostępu do bazy danych w projekcie.
Ustawić odpowiednie dane w pliku .env

3. Zmiana uprawnień dla ktalogów i plików. Jeśli uprawnienia są ograniczone dla grupy i wszystkich pozostałych.

sudo docker exec -it mysql-company bash

find /var/www/ -type d -exec chmod 777 {} +
find /var/www/ -type f -exec chmod 666 {} +

chown -hR 1000:1000 /var/www/

chmod 644 /var/www/docker/config/my.cnf
chmod 755 /root/*.sh
